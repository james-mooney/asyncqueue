﻿using System;
using System.Threading;

namespace AsyncQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            var queue = new AsyncQueue();

            for (int i = 0; i < 10; i++)
            {
                int itemNumber = i;
                queue.EnqueueItem(() =>
                {
                    // Checks a slowrunning thread doesn't get "overtaken"
                    if (itemNumber == 2)
                    {
                        Thread.Sleep(1000);
                    }
                    // Simulate time-consuming work
                    Thread.Sleep(100);
                    Console.Write(" Task" + itemNumber);
                });
            }

            // Testing the responsiveness of the UI
            Console.WriteLine("UI responsiveness test.");
            var value = Console.ReadLine();
            Console.WriteLine(value);

            // Testing additional items are being added to queue whilst running
            for (int i = 10; i < 20; i++)
            {
                int itemNumber = i;
                queue.EnqueueItem(() =>
                {
                    // Simulate time-consuming work
                    Thread.Sleep(100);
                    Console.Write(" Task" + itemNumber);
                });
            }

            // Testing queue restarts
            Console.ReadLine();
            for (int i = 20; i < 22; i++)
            {
                int itemNumber = i;
                queue.EnqueueItem(() =>
                {
                    // Simulate time-consuming work
                    Thread.Sleep(1000);
                    Console.Write(" Task" + itemNumber);
                });
            }

            Console.Read();
        }
    }
}
