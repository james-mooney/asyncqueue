﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace AsyncQueue
{
    public class AsyncQueue
    {
        ConcurrentQueue<Action> queue = new ConcurrentQueue<Action>();
        bool isRunning;

        public void EnqueueItem(Action item)
        {
            queue.Enqueue(item);

            if (!isRunning)
            {
                isRunning = true;
                Task.Run(() => Consume());
            }
        }

        private void Consume()
        {
            while (queue.TryDequeue(out Action item))
            {
                item();
            }

            isRunning = false;
        }
    }
}
